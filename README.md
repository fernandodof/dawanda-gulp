# README #

# How do I get set up?

Clone this repo

Go to its folder

$ npm install

$ bower install

### For the development server 

$ gulp run

(Go to http://localhost:7083/src/)

### For the Production server

$ gulp build (build files will go to dist folder)

$ gulp run-dist

(Go to http://localhost:8888)