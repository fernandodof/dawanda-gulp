var gulp = require('gulp'),
    usemin = require('gulp-usemin'),
    connect = require('gulp-connect'),
    minifyJs = require('gulp-uglify'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    cleanCSS = require('gulp-clean-css'),
    http = require('http'),
    ecstatic = require('ecstatic');

var paths = {
    root: 'src',
    scripts: 'src/app/**/*.js',
    images: 'src/img/**/*.*',
    favicon: 'src/favicon.ico',
    index: 'src/index.html',
    bower_fonts: 'bower_components/**/*.{ttf,woff,woff2,eof,svg}',
    styles: 'src/styles/sass/**/*.scss',
    styles_compiled: 'src/styles/compiled/'
};

var ports = {
    dev: '7083'
};

// Task to watch .scss files and rebuild css
gulp.task('watch-dev', function() {
    gulp.watch([paths.styles], ['custom-sass-dev']);
});

//Task to complie sass files
gulp.task('custom-sass-dev', function() {
    return gulp.src(paths.styles)
        .pipe(sass()
            .on('error', sass.logError))
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(gulp.dest(paths.styles_compiled));
});

//Taks to create development server
gulp.task('run-dev', function() {
    http.createServer(
        ecstatic({
            root: __dirname + '/'
        })
    ).listen(ports.dev);

    console.log(
        'Listening on http://localhost:' +
        ports.dev + '/' +
        paths.root);
});

//Bellow task are for production

//Task for concat and minfy javascript and css files
gulp.task('dist-usemin', function() {
    return gulp.src(paths.index)
        .pipe(usemin({
            js: [minifyJs(), 'concat'],
            css: [cleanCSS({
                compatibility: 'ie8'
            }), 'concat'],
        }))
        .pipe(gulp.dest('dist/'));
});

//Task to copy fonts to dist folder
gulp.task('dist-bower-fonts', function() {
    return gulp.src(paths.bower_fonts)
        .pipe(rename({
            dirname: '/fonts'
        }))
        .pipe(gulp.dest('dist'));
});

//Task to copy images to dist folder
gulp.task('dist-images', function() {
    return gulp.src(paths.images)
        .pipe(gulp.dest('dist/img'));
});

//Task to copy js files to dist folder
gulp.task('dist-js', function() {
    return gulp.src(paths.scripts)
        .pipe(minifyJs())
        .pipe(concat('custom.min.js'))
        .pipe(gulp.dest('dist/js'));
});

//Task to complie css to dist folder
gulp.task('dist-sass', function() {
    return gulp.src(paths.styles)
        .pipe(sass()
            .on('error', sass.logError))
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(gulp.dest('dist/styles/compiled'));
});

//Task to copy favicon to dist folder
gulp.task('dist-favicon', function() {
    return gulp.src(paths.favicon)
        .pipe(gulp.dest('dist'));
});

//Task to create dist server
gulp.task('webserver', function() {
    connect.server({
        root: 'dist',
        livereload: true,
        port: 8888
    });
});

// Task for development server
gulp.task('run', ['run-dev', 'watch-dev']);

//Task to build dist files
gulp.task('build', ['dist-usemin', 'dist-bower-fonts', 'dist-images', 'dist-js', 'dist-sass', 'dist-favicon'])

//Task for dist server
gulp.task('run-dist', ['webserver']);
